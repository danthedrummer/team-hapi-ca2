package com.teamh.teamhapi.model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Model describing the server response to any get book requests
 * e.g. {@link Catalog#getAllBooks()} and {@link Catalog#getBookById(long)}
 * 
 * @author Glenn Cullen
 * @author Dan Downey
 * @author Aaron Meaney
 * @author Paul Reid
 */
@XmlRootElement(name = "response")
public class GetBooksResponse {
    
    private List<Book> books;

    public GetBooksResponse() {
    }

    public GetBooksResponse(List<Book> books) {
        this.books = books;
    }
    
    public GetBooksResponse(final Book book) {
        this.books = new ArrayList<Book>(){{
            add(book);
        }};
        
    }

    @XmlElement
    public List<Book> getBooks() {
        return books;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }
    
}

package com.teamh.teamhapi.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Model describing the server response to a book deletion request
 * 
 * @author Glenn Cullen
 * @author Dan Downey
 * @author Aaron Meaney
 * @author Paul Reid
 */
@XmlRootElement(name = "response")
public class DeleteBookResponse {
    
    private Book book;
    private boolean deletedStatus;

    public DeleteBookResponse(boolean deletedStatus) {
        this.deletedStatus = deletedStatus;
    }
    
    public DeleteBookResponse() {
    }

    public DeleteBookResponse(Book book, boolean deletedStatus) {
        this.book = book;
        this.deletedStatus = deletedStatus;
    }

    @XmlElement
    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    @XmlElement(name = "success")
    public boolean isDeletedStatus() {
        return deletedStatus;
    }

    public void setDeletedStatus(boolean deletedStatus) {
        this.deletedStatus = deletedStatus;
    }
        
}

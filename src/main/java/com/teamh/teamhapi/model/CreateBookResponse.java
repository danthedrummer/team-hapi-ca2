package com.teamh.teamhapi.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Model describing the server response to a book creation request
 * 
 * @author Glenn Cullen
 * @author Dan Downey
 * @author Aaron Meaney
 * @author Paul Reid
 */
@XmlRootElement(name = "response")
public class CreateBookResponse {
    
    private boolean success;
    private String message;

    public CreateBookResponse() {
    }

    public CreateBookResponse(boolean success, long bookId) {
        this.success = success;
        this.message = bookId != -1 
                ? "New book available at: http://localhost:49000/api/catalog/book/"+bookId
                : "Could not create new resource as that book already exists";
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @XmlElement
    public boolean isSuccess() {
        return success;
    }

    @XmlElement
    public String getMessage() {
        return message;
    }

    
}

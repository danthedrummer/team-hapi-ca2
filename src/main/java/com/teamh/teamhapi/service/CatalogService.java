package com.teamh.teamhapi.service;

import com.teamh.teamhapi.model.Book;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Glenn Cullen
 * @author Dan Downey
 * @author Aaron Meaney
 * @author Paul Reid
 */
public enum CatalogService {
    INSTANCE;
    
    /**
     * Creating an ArrayList that stores books. Using double brace
     * initialization to easily pre-populate the list upon creation
     */
    private ArrayList<Book> bookList = new ArrayList<Book>(){{
        add(new Book(0,"Francis of the Filth","George Miller","Lulu"));
        add(new Book(1,"Masters of Doom","David Kushner","Random House"));
        add(new Book(2,"Year Zero","Rob Reid","Ballantine Books"));        
    }};
    
    /**
     * Gets all the books from the catalog
     * @return The list of books we are storing in bookList
     */
    public List<Book> getAllBooks() {
        return bookList;
    }
    
    /**
     * Gets a single book by it's id
     * @param id the id of the book
     * @return the book with the specified id
     */
    public Book getBook(long id){
        for(Book book : bookList){
            if(book.getId() == id){
                return book;
            }
        }
        return null;
    }
    
    /**
     * Creates a new book 
     * @param newBook the new book to be added to the catalog
     * @return the id of the newly added book, or -1 if the book already exists
     */
    public long createBook(Book newBook){
        for (Book book : bookList) {
            if (book.getTitle().equals(newBook.getTitle())
                    && book.getAuthor().equals(newBook.getAuthor())
                    && book.getPublisher().equals(newBook.getPublisher())) {
                return -1;
            }
        }
        newBook.setId(bookList.get(bookList.size()-1).getId()+1);
        bookList.add(newBook);
        return newBook.getId();
    }
    
    /**
     * Deletes the book with the specified id
     * @param id the id of the book to be deleted
     * @return the book that was deleted, null otherwise
     */
    public Book deleteBook(long id) {
        for (Book book : bookList) {
            if (book.getId() == id) {
                Book temp = book;
                bookList.remove(book);
                return temp;
            }
        }
        return null;
    }
    
}

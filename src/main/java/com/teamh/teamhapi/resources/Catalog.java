package com.teamh.teamhapi.resources;

import com.teamh.teamhapi.model.GetBooksResponse;
import com.teamh.teamhapi.service.CatalogService;
import com.teamh.teamhapi.model.Book;
import com.teamh.teamhapi.model.CreateBookResponse;
import com.teamh.teamhapi.model.DeleteBookResponse;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Catalog resource class for Web Services and API Development CA2
 * 
 * @author Glenn Cullen
 * @author Dan Downey
 * @author Aaron Meaney
 * @author Paul Reid
 */
@Path("/catalog")
@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
@Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
public class Catalog {
    
    @GET
    @Path("/books")
    public Response getAllBooks() {
        List<Book> temp = CatalogService.INSTANCE.getAllBooks();
        return Response.status(200)
                .entity(new GetBooksResponse(temp))
                .build();
    }
    
    @GET
    @Path("/book/{id}")
    public Response getBookById(@PathParam("id") long id) {
        System.out.println("Getting book by id: " + id);
        Book temp = CatalogService.INSTANCE.getBook(id);
        if(temp == null){
            return Response.status(404).build();
        }
        return Response.status(200).entity(new GetBooksResponse(temp)).build();
    }
    
    @POST
    @Path("/book")
    public Response createBook(Book newBook) {
        long id = CatalogService.INSTANCE.createBook(newBook); 
        
        if (id == -1) {
            return Response.status(409)
                    .entity(new CreateBookResponse(false, id))
                    .build();
        }
        
        return Response.status(201)
                .entity(new CreateBookResponse(true, id))
                .build();
    }
    
    @DELETE
    @Path("/book/{id}")
    public Response deleteBook(@PathParam("id")long id) {
        Book temp = CatalogService.INSTANCE.deleteBook(id);
        if(temp == null){
            return Response.status(404)
                    .entity(new DeleteBookResponse(false))
                    .build();
        }
        return Response.status(200)
                .entity(new DeleteBookResponse(temp, true))
                .build();
    }
}
